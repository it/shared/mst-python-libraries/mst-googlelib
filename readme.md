# mst-googlelib

[[_TOC_]]

## Installation

Command Line:

```bash
pip install mst-googlelib --index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
```

requirements.txt

```text
--extra-index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
mst-googlelib
```

## Usage

mst-googlelib is a wrapper around the Google Admin SDK libraries, using a model based approach to manage resources.

```python
from mst.googlelib import GoogleSDK, models

google_json = "<snip>"

google_sdk = GoogleSDK()
google_sdk.init(google_json, "listmgr")

group_key = f"my-group@{google_sdk.group_domain}"

# Load existing group with email `group_key`
my_group = models.Group(group_key)

# Delete the group
my_group.delete()

# Create the group
my_group = models.Group(group_key).create()

```
