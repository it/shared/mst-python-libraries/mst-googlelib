import datetime
import os
import time
import setuptools

VERSION = "2.0.2"

if os.getenv("MST_DEVELOPMENT", ""):
    stamp = os.getenv("CI_PIPELINE_CREATED_AT", "")
    if stamp:
        # Convert the given timestamp to seconds to mark as version.
        suffix = round(datetime.datetime.fromisoformat(stamp).timestamp())
    else:
        # Convert back to seconds and do this to stop it from changing versions mid-build (which can happen.)
        suffix = {time.time_ns() // 1000000000 // 20}
    VERSION = f"{VERSION}.dev{suffix}"

setuptools.setup(version=VERSION)
